import './App.css';
import ScrollableTabs from './components/ScrollableTabs'
import Kzarim from "./components/forms/Kzarim";
import { Route, Routes } from "react-router-dom";
function App() {
  const routes = [
    { route: "/kzarim", component: Kzarim, label: "item one" },
    { route: "/unknown", component: Kzarim, label: "item two" },
    { route: "/", component: null, label: "item three" },
  ];
  return (
    <div className="App">
      <header className="App-header">
        <ScrollableTabs routes={routes}/>
        <Routes>
          {routes.map((item) => {
            return (
              <Route
                key={item.route}
                path={item.route}
                Component={item.component}
              ></Route>
            );
          })}
        </Routes>
      </header>
    </div>
  );
}

export default App;
