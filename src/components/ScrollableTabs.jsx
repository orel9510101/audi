import * as React from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import Kzarim from "./forms/Kzarim";
import { Route, Routes, Link } from "react-router-dom";

export default function ScrollableTabs(routesArray) {
  const [value, setValue] = React.useState(0);
  const routes = routesArray.routes;
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%", backgroundColor: "#282c34" }}>
      <Tabs value={value} onChange={handleChange} centered>
        {routes.map((item) => {
          return (
            <Tab
              label={item.label}
              key={item.route}
              to={item.route}
              component={Link}
            />
          );
        })}
      </Tabs>
    </Box>
  );
}
