import React, { useState } from "react";
import { Box, TextField } from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import FingerprintIcon from "@mui/icons-material/Fingerprint";
import ScheduleSendIcon from "@mui/icons-material/ScheduleSend";
import PhoneCallbackIcon from "@mui/icons-material/PhoneCallback";
import PhoneForwardedIcon from "@mui/icons-material/PhoneForwarded";
import HourglassEmptyIcon from "@mui/icons-material/HourglassEmpty";
import BasicInput from "./basicInput";

export default function Kzarim() {
  const [id, setId] = useState("");
  const [bam1, setBam1] = useState("");
  const [bam2, setBam2] = useState("");
  const [bam3, setBam3] = useState("");
  const [loading, setLoading] = React.useState(false);
  const list = [
    {
      id: "id",
      label: "label",
      component: (
        <FingerprintIcon sx={{ color: "action.active", mr: 1, my: 0.5 }} />
      ),
    },
    {
      id: "bam1",
      label: "bam1",
      component: (
        <PhoneForwardedIcon sx={{ color: "action.active", mr: 1, my: 0.5 }} />
      ),
    },
    {
      id: "bam2",
      label: "bam2",
      component: (
        <PhoneCallbackIcon sx={{ color: "action.active", mr: 1, my: 0.5 }} />
      ),
    },
    {
      id: "bam3",
      label: "bam3",
      component: (
        <HourglassEmptyIcon sx={{ color: "action.active", mr: 1, my: 0.5 }} />
      ),
    },
  ];
  function handleSubmit(e) {}

  function handleClick() {
    setLoading(true);
  }

  return (
    <div>
      {list.map((item) => {
        return (
          <BasicInput
            id={item.id}
            label={item.label}
            component={item.component}
            key={item.id}
          ></BasicInput>
        );
      })}
      <LoadingButton
        onClick={handleClick}
        endIcon={<ScheduleSendIcon />}
        loading={loading}
        loadingPosition="end"
        variant="contained"
      >
        <span>Send</span>
      </LoadingButton>
    </div>
  );
}
