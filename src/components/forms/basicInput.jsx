import React, { useState } from "react";
import { Component } from "react";
import { Box, TextField } from "@mui/material";
import "./basicinput.css";
export default function BasicInput({ component, externalId, label }) {
  const [id, setId] = useState(externalId);

  return (
    <Box sx={{ display: "flex", alignItems: "flex-end" }}>
      {component}
      <TextField
        className="input-text"
        id={id}
        label={label}
        variant="standard"
        onChange={(e) => setId(e.target.value)}
      />
    </Box>
  );
}
